def parsemsg(text):
    """Breaks a message from an IRC server into its prefix, command, and arguments.
    https://stackoverflow.com/questions/930700/python-parsing-irc-messages
    """
    s = text.strip()
    prefix = ''
    # trailing = []
    # if not s:
    #    raise IRCBadMessage("Empty line.")
    if s[0] == ':':
        prefix, s = s[1:].split(' ', 1)
    if s.find(' :') != -1:
        s, trailing = s.split(' :', 1)
        args = s.split()
        args.append(trailing)
    else:
        args = s.split()
    command = args.pop(0)
    return prefix, command, args


class MsgParser:
    def __init__(self,text):
        self._text=text
        self.prefix,self.command,self.args=parsemsg(text)


    def get_nick(self):
        nick=""
        if self.prefix:
            nick=self.prefix.split('!',1)[0]
        return nick

    def get_ip_nick_userhost(self):
        ip=""
        try:
            if self.prefix:
                ip = self.args[1].split('@', 1)[1].strip()
                if ip:
                    ip =ip.split(' ', 1)[0].strip()
        except:
            ip=""
        return ip