from lxml import html
import requests

# Define the search function of the xpath
def get_info_package(namepkg):
    result=[]
    try:
        page = requests.get(f'https://packages.debian.org/{namepkg}')
        tree = html.fromstring(page.content)
        stable = tree.xpath('//*[@id="psearchres"]/ul[1]/li[contains(@class,"bookworm")]')
        testing = tree.xpath('//*[@id="psearchres"]/ul[1]/li[contains(@class,"trixie")]')
        sid = tree.xpath('//*[@id="psearchres"]/ul[1]/li[contains(@class,"sid")]')
        x=stable[0].text_content().split('\n')
        y=[a.strip() for a in x if a.strip()]
        result.append(f"{namepkg}: {y[1]}")
        result.append(y[0]+ " " +y[2])
        x=testing[0].text_content().split('\n')
        y=[a.strip() for a in x if a.strip()]
        result.append(y[0]+ " " +y[2])
        x=sid[0].text_content().split('\n')
        y=[a.strip() for a in x if a.strip()]
        result.append(y[0]+ " " + y[2])
    except IndexError:
        result=["Unknown Packet"]
        return result

    return result


if __name__ == "__main__":
    print(get_info_package('mc'))
