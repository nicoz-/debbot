import socket
import sys
import ssl
import time
from msg_parser import MsgParser


_LAST_PING_MAX_SECONDS=180
_LAST_RESPONSE_MAX_SECONDS=480

class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class BotError(Error):
    def __init__(self, expression, message):
        self.expression = expression
        self.message = message

class IRC:

    def __init__(self,family,logger):
        # Define the socket

        self.irc = socket.socket()
        self.irc = socket.socket(family, socket.SOCK_STREAM)
        self.timeout=5
        self.logger=logger
        self.last_pong_replay =""
        self.last_ping=time.time()
        self.last_response = time.time()

    def settimeout(self,timeout):
        '''calculation function for timeout'''
        tmot=max(5,timeout)
        tmot=min(tmot,max(_LAST_PING_MAX_SECONDS-(time.time()-self.last_ping),1))
        self.timeout=tmot


    def send_msg(self, channel, msg):
        '''send message function...'''
        # Transfer data
        msg_sent="PRIVMSG " + channel + " :" + msg + "\n"
        self.irc.send(bytes(msg_sent, "UTF-8"))
        self.logger.info(("--> "+msg_sent).strip())
    

    def send_mode(self, channel, operation, nick_rec):
        '''send mode operator function'''
        match operation:
            case "op":
                mode = "MODE " + channel + " +o " + nick_rec + "\n"
                self.irc.send(bytes(mode, "UTF-8"))
                self.logger.info(("--> "+mode).strip())
            case "deop":
                mode = "MODE " + channel + " -o " + nick_rec + "\n"
                self.irc.send(bytes(mode, "UTF-8"))
                self.logger.info(("--> "+mode).strip())
            case "kick":
                mode = "KICK " + channel + " " + nick_rec +"\n"
                self.irc.send(bytes(mode, "UTF-8"))
                self.logger.info(("--> "+mode).strip)
            case "kickban":
                mode = "MODE " + channel + " +b " + nick_rec +"\n"
                self.irc.send(bytes(mode, "UTF-8"))
                self.logger.info(("--> "+mode).strip)
                time.sleep(1)
                mode = "KICK " + channel + " " + nick_rec +"\n"
                self.irc.send(bytes(mode, "UTF-8"))
                self.logger.info(("--> "+mode).strip)


    def connect(self, ssl_connection, ipv6, server, port, channel, botnick, botnickpass, botpass):
        '''Function to establish connection with IRC server'''
        
        self.logger.info("Connecting to: " + server)
        if not ipv6:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)        

        if not ssl_connection:
            try:
                sock.connect((server, port))
                self.irc = sock
                self.logger.info(f"Connected to {server} without SSL.")
                
                self.irc.send(bytes(f"USER {botnick} {botnick} {botnick} :Python debbot for debian-it\n", "UTF-8"))
                self.irc.send(bytes(f"NICK {botnick}\n", "UTF-8"))

                if botnickpass:
                    self.irc.send(bytes(f"NICKSERV IDENTIFY {botnickpass} {botpass}\n", "UTF-8"))
                    time.sleep(10)

                time.sleep(5)
                self.irc.send(bytes(f"JOIN {channel}\n", "UTF-8"))

            except socket.error as e:
                self.logger.error(f"Socket error: {e}")
                sock.close()

        else:
            context = ssl.create_default_context() # default context
            context.check_hostname = False
            context.verify_mode = ssl.CERT_NONE  # edit this if you would use your cert and change context.check_hostname in True
            ssl_sock = context.wrap_socket(sock, server_hostname=server)

            try:
                ssl_sock.connect((server, port))
                self.irc = ssl_sock
                self.logger.info(f"Connected to {server} with SSL.")
                
                self.irc.send(bytes(f"USER {botnick} {botnick} {botnick} :Python debbot for debian-it\r\n", "UTF-8"))
                self.irc.send(bytes(f"NICK {botnick}\r\n", "UTF-8"))

                if botnickpass:
                    self.irc.send(bytes(f"NICKSERV IDENTIFY {botnickpass} {botpass}\r\n", "UTF-8"))
                    time.sleep(10)

                time.sleep(5)
                self.irc.send(bytes(f"JOIN {channel}\r\n", "UTF-8"))

            except ssl.SSLError as e:
                self.logger.error(f"SSL error: {e}")
                ssl_sock.close()


    def get_response(self):
        '''function for getting response'''
        time.sleep(1)
        self.logger.info("Waiting for response")
        self.irc.settimeout(self.timeout)

        try:
            resp = self.irc.recv(4096).decode("UTF-8")
            if not resp:
                raise BotError('BotError1', 'BotError1: Empty socket response. Stopping bot...')
            self.logger.info(("<-- " + resp).strip())
            self.last_response = time.time()
            
            parsed_msg = MsgParser(resp)
            if parsed_msg.command == 'PING':
                msg1 = ':' + parsed_msg.args[0]
                self.pong(msg1)
        
        except socket.timeout as e:
            resp = "Timeout: " + str(e)
            parsed_msg = MsgParser(resp)
            self.logger.info(resp)
            self.time_out_handler()

        except socket.error as e:
            msg = f"BotError2: Socket error: {e}. Stopping bot..."
            self.logger.error(msg)
            
            
            raise BotError("BotError2", msg)

        except BotError as e:
            # Handle custom BotError
            self.logger.error(e.message)
            raise e
        
        return parsed_msg


    def time_out_handler(self):
        if time.time() - self.last_response > _LAST_RESPONSE_MAX_SECONDS:
            self.logger.error("No response for too long. The bot may be disconnected. Stopping bot...")
            raise BotError('BotError3', 'BotError3: No response for too long. The bot may be disconnected. Stopping bot...')
        # I check last ping and if more than _LAST_PING_MAX_SECONDS seconds have passed
        # I behave as if I have missed it
        if time.time() - self.last_ping > _LAST_PING_MAX_SECONDS:
            self.logger.info("Suspect missed ping")
            self.pong("")


    def pong(self,pong_reply):
        '''ping pong function'''
        pr=pong_reply
        if not pr:
            # I probably missed some pings and try to fix it
            self.logger.info("Try to fix missed ping")
            pr=self.last_pong_replay

        msg = 'PONG ' + pr + '\r\n'
        self.irc.send(bytes(msg, "UTF-8"))
        self.logger.info(("--> " + msg+" ").strip())
        self.last_pong_replay=pr
        self.last_ping=time.time()


    def info_nick(self, nick):
        '''function for extract host/ip of user'''
        msg_sent="USERHOST " + nick + "\n"
        self.irc.send(bytes(msg_sent, "UTF-8"))
        self.logger.info(("Request nick info --> "+msg_sent).strip())
        time.sleep(1)
        resp = self.irc.recv(4096).decode("UTF-8")
        self.logger.info(("Nick info <-- " + resp).strip())
        parsed_msg = MsgParser(resp)
        result=parsed_msg.get_ip_nick_userhost()
        return result
