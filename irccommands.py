#!/usr/bin/env python3

import json,re
from irc_class import *
import ipcommand, pckg, irc_bot, getfeeds
import subprocess
import datetime

#this class allows you to add some simple commands using a json command file

class IRC_commands:


    def __init__(self,command_file,irc_instance=None,logger=None,channel=None,botnick=None,bot_connection_time=None):
        self._command_file = command_file
        self._commands=self.read_commands(command_file)
        self._pattern = re.compile(r"!([a-zA-Z0-9]+)")  # old regex re.compile(r"!([a-zA-Z0-9]+)$") not accept space
        self.irc_instance = irc_instance
        self._logger=logger
        self.channel = channel
        self.botnick = botnick
        self.bot_uptime = bot_connection_time
        self.dsa_url = "https://www.debian.org/security/dsa-long"
        self.planet_url = "https://planet.debian.org/rss20.xml"


    def ip_command(self, arg):
        '''function for resolve hostname of user or hostname info'''
        if arg==self.botnick:
            #hide bot info
            return
        result=ipcommand.check_ip_command(arg)
        if not result:
            arg1=self.irc_instance.info_nick(arg)
            if arg1:
                result = ipcommand.check_ip_command(arg1)
                if result:
                    result.insert(0,f'Nick: \002{arg}\002 ; Host: \002{arg1}\002')
        if result:
            for x in result:  
                self.irc_instance.send_msg(self.channel, x) 
        # if not resolve, try to resolve on terminal
        elif result == []:
            res = subprocess.run(["host",f"{arg}"],stdout=subprocess.PIPE,stderr=subprocess.PIPE, encoding="utf-8")
            fres = res.stdout.split(" ")[4].strip()
            if fres not in ["3(NXDOMAIN)", "127.0.0.1"]:
                result = ipcommand.check_ip_command(fres)
                if result:
                    for x in result:
                        self.irc_instance.send_msg(self.channel, x)
            else:
                self.irc_instance.send_msg(self.channel, "unknown host/nick")


    def dpkg_command(self, arg):
        '''function to call pckg'''
        result=pckg.get_info_package(arg)
        for x in result:
            self.irc_instance.send_msg(self.channel, x.replace("\n", " "))


    def get_cpu(self):
        '''function to recognize and print cpu info on channel'''
        cpu = subprocess.run(["lscpu", "-J"],stdout=subprocess.PIPE,stderr=subprocess.PIPE, encoding="utf-8")
        sub_cpu = cpu.stdout
        dict_cpu = json.loads(sub_cpu)

        #architecture
        arch = str(dict_cpu['lscpu'][0].values())
        sub_arch = arch[13:][:-2].replace(",","").replace("'","")

        #Cores
        get_cores = str(dict_cpu['lscpu'][3].values())
        cores = get_cores[13:][:-2].replace(",","").replace("'","")

        #Model
        get_cpu_mod = str(dict_cpu['lscpu'][10].values())
        model_cpu = get_cpu_mod[13:][:-2].replace(",","").replace("'","")

        #Build a msg
        final_reply = sub_arch+', '+cores+', '+model_cpu+'.'
        return (final_reply)


    def get_bot_uptime(self):
        '''function to get the bot uptime'''
        now = datetime.datetime.now(datetime.timezone.utc)
        delta = now - self.bot_uptime
        hours, remainder = divmod(int(delta.total_seconds()), 3600)
        minutes, seconds = divmod(remainder, 60)
        days, hours = divmod(hours, 24)


        if days:
            fmt = '{d} days, {h} hours, {m} minutes, and {s} seconds'
        else:
            fmt = '{h} hours, {m} minutes, and {s} seconds'

        return fmt.format(d=days, h=hours, m=minutes, s=seconds)


    def is_raspberry_pi(self):
        '''function to recognize if is a RaspBerry'''
        try:
            with open("/proc/cpuinfo", "r") as f:
                cpuinfo = f.read()
            if "Raspberry Pi" in cpuinfo or "BCM" in cpuinfo:
                return True
        except FileNotFoundError:
            return False
        return False


    def get_temperature(self):
        '''this function is only for raspberry'''
        rasp = self.is_raspberry_pi()
        if rasp is True:
            temp = subprocess.run(["vcgencmd","measure_temp"],stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8")
            sub_temp = temp.stdout[:5]
            return str(sub_temp)
        else:
            return "This command is only available only on RaspBerry!"


    def read_commands(self,fn):
        with open(fn, "r") as read_file:
            data = json.load(read_file)
        return data
    

    def execute(self,key,command,nick,parsed_message):
        sc=""
        admin = self._commands.get("admin", {}).get("adm", [])
        nick_cmd = parsed_message.args[1].split(' ')[1:]
        
        
        # public command (for all users)
        
        match key:
            case "dsa":
                sc=irc_bot.print_feeds(self._logger, self.irc_instance, self.channel, getfeeds.get_feeds(self.dsa_url))
                return
            case "planet":
                sc=irc_bot.print_feeds(self._logger, self.irc_instance, self.channel, getfeeds.get_feeds(self.planet_url))
            case "ip":
                sc=self.ip_command(nick_cmd[0])
                return sc
            case "dpkg":
                sc=self.dpkg_command(nick_cmd[0])
                return sc
            case "botuptime":
                sc=self.get_bot_uptime()
                self.irc_instance.send_msg(self.channel,sc)
                return
            case "cpu":
                sc=self.get_cpu()
                if sc:
                    self.irc_instance.send_msg(self.channel,sc)
                else:
                    return
        # admin command 
        if (nick in admin) or ("*" in admin):
            match key:
                case "reload":
                    if (nick in command.get("allow")) or ("*" in command.get("allow")):
                        self._commands = self.read_commands(self._command_file)
                        sc="Command file reloaded"
                case "op":
                    if nick_cmd:
                        sc = self.irc_instance.send_mode(self.channel, "op", nick_cmd[0])
                        self.log(f"Granting operator status to {nick_cmd[0]} in channel {self.channel}")
                case "deop":
                    if (nick_cmd[0] != self.botnick) and (nick_cmd[0] not in admin):
                        sc = self.irc_instance.send_mode(self.channel, "deop", nick_cmd[0])
                case "kick":
                    if (nick_cmd[0] != self.botnick) and (nick_cmd[0] not in admin):
                        sc = self.irc_instance.send_mode(self.channel, "kick", nick_cmd[0])
                case "kickban":
                    if (nick_cmd[0] != self.botnick) and (nick_cmd[0] not in admin):
                        sc = self.irc_instance.send_mode(self.channel, "kickban", nick_cmd[0])
                case _:
                    sc=command.get("show")
            return sc
        else:
            self.irc_instance.send_msg(self.channel, f"{nick}: non sei autorizzato a usare questo comando")
            self.log(f"REJECT CMD --> {nick} do not have permission to use this command.")


    def check_and_execute(self, parsed_message,botnick):
        result=None
        key,cmd = self.check(parsed_message,botnick)
        if cmd:
            result=self.execute(key,cmd,parsed_message.get_nick(), parsed_message)
        return result

    def check(self, parsed_message, botnick):
        key, cmd = None, None
        if parsed_message.command == "PRIVMSG" and botnick != parsed_message.get_nick():
            s = parsed_message.args[1]
            self.log(f"Received string for command matching: {s}") # log for debug error
            m = self._pattern.search(s.lower()) # turn the str into minuscule 
            self.log(f"Regex match result: {m}") # log for debug error
            if m is not None and len(m.groups()) == 1:
                pcmd = m.group(1)
                self.log(f"Check command candidate pattern found: {pcmd}")
                if pcmd in self._commands:
                    key, cmd = pcmd, self._commands[pcmd]
                else:
                    self.log(f"Command {pcmd} not found in commands list")
            else:
                self.log("No command match found")
        return key, cmd


    def log(self,msg):
        if self._logger:
            self._logger.info(msg)
        else:
            print(msg)


