#!/usr/bin/env python3

# This bot reads news from a couple of sources
# I found here the basic bot code: https://www.techbeamers.com/create-python-irc-bot/
# on this I have built my news reader

from irc_class import *
import os,time
import datetime
import getfeeds ,irccommands
import configparser
import argparse
import subprocess
import json
import logging, logging.handlers
from msg_parser import MsgParser


def print_feeds(logger,irc,channel,feeds):
    '''function print news on channel'''
    logger.info("print_feeds")
    for feed in reversed(feeds):
        irc.send_msg(channel, feed)
        time.sleep(1)

def check_news():
    '''function check last news feeds'''
    logger.info("check_news")
    s=dsa.get_new_feeds()
    print_feeds(logger, irc, channel, s)
    s=planet.get_new_feeds()
    print_feeds(logger,irc, channel, s)


def get_logger(fn):
    '''function call logger'''
    logger = logging.getLogger('debbot')
    logger.setLevel(logging.DEBUG)
    handler = logging.handlers.TimedRotatingFileHandler(fn, when='d', interval=1, backupCount=7)
    formatter = logging.Formatter('%(asctime)s - %(name)s: %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


if __name__ == "__main__":

    BOT_VERSION="1.7.3"
    RELEASE_DATE="2024-09-29"
    version_str=f"Deb-Bot {BOT_VERSION} ({RELEASE_DATE})"
    cf=os.path.join(os.path.dirname(sys.argv[0]),'debbot.conf')
    lf = os.path.join(os.path.dirname(sys.argv[0]), 'debbot.log')
    cmdfile = os.path.join(os.path.dirname(sys.argv[0]), 'irc_commands.json')

    parser = argparse.ArgumentParser(description='Debian News Irc Bot')
    parser.add_argument('-c','--config', type=str,default=cf)
    parser.add_argument('-l', '--logfile', type=str, default=lf)
    parser.add_argument( '--commandfile', type=str, default=cmdfile)
    parser.add_argument('--version', action='version', version=version_str)
    args = parser.parse_args()

    conf=args.config
    logger=get_logger(args.logfile)

    cp=configparser.ConfigParser()
    cp.read(conf)

    ## IRC Config those in the conf file
    ssl_connection=cp['DEFAULT']['ssl']=='Y'
    server=cp['DEFAULT']['server']
    port = int(cp['DEFAULT']['port']) 
    channel = cp['DEFAULT']['channel'] 
    botnick=cp['DEFAULT']['botnick']
    botnickpass=cp['DEFAULT']['botnickpass']
    botpass=cp['DEFAULT']['botpass']
    ipv6=cp['DEFAULT']['ipv6']=='Y'
   

    if ipv6:
        family = socket.AF_INET6
    else:
        family = socket.AF_INET

    check_every_secs=int(cp['DEFAULT']['check_every_secs'])#300
    t=time.time()-10000

    irc = IRC(family,logger)
    #commands
    command_file=args.commandfile
    bot_connection_time=datetime.datetime.now(datetime.UTC)

    try:
        commands=irccommands.IRC_commands(command_file,irc,logger,channel,botnick,bot_connection_time)
    except Exception as e:
        logger.error(f"Problem loading commmand file {command_file}: {e}")
        commands = None

    #connection
    irc.connect(ssl_connection, ipv6, server, port, channel, botnick, botnickpass, botpass) 
    
    #feed sources
    dsa_url = "https://www.debian.org/security/dsa-long"
    planet_url = "https://planet.debian.org/rss20.xml"
    dsa=getfeeds.GetNewFeeds(dsa_url,"dsa")
    planet=getfeeds.GetNewFeeds(planet_url,"planet")


    loopCondition=True

    while loopCondition:
        irc.settimeout(check_every_secs-(time.time()-t)+1)
        parsed_msg= irc.get_response()
        
        if parsed_msg.command=="PRIVMSG" and channel==parsed_msg.args[0]:
            arg2 = parsed_msg.args[1]
            match arg2:
                case "hello":
                    irc.send_msg(channel, f"Hello {parsed_msg.get_nick()}!")
                case "!botversion":
                    irc.send_msg(channel, version_str)


            if commands:
                cmd=commands.check_and_execute(parsed_msg,botnick)
                if cmd:
                    irc.send_msg(channel, cmd)
                
        if time.time()-t > check_every_secs:
            check_news()
            t = time.time()
