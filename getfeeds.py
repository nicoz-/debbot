#!/usr/bin/env python3

import feedparser
import sys,re,time,os

def get_feeds(url):
    d = feedparser.parse(url)
    # ce=d['entries'][0] #current entry
    result=[]
    conta=0
    for ce in d['entries']:
        conta+=1
        dateup=""
        if ce.has_key('updated'):
            dateup=ce['updated']
        elif ce.has_key('published'):
            dateup=ce['published']
        result.append( dateup+" " +ce['title']+" "+ce['link']) #+ce['summary'])
        if conta>=3:
            break
    #.feed.subtitle
    return result

class GetNewFeeds:
    def __init__(self, url,name,last_check=""):
        self.url = url
        self.name = name
        if last_check=="":
            self.read_status()
        else:
            self.last_check=last_check
    ...
    def get_new_feeds(self):
        d = feedparser.parse(self.url)
        # ce=d['entries'][0] #current entry
        result=[]
        current_last_check=self.last_check
        tmp_last_check = self.last_check
        for ce in d['entries']:
            current_last_check=self.get_last_check(ce)
            if current_last_check>self.last_check:
                dateup=""
                if ce.has_key('updated'):
                    dateup=ce['updated']
                elif ce.has_key('published'):
                    dateup=ce['published']
                result.append( dateup+" " +ce['title']+" "+ce['link']) #+ce['summary'])

                if current_last_check> tmp_last_check:
                    tmp_last_check=current_last_check
            else:
                break
        if self.last_check=='':
            result=result[:3]
        if tmp_last_check > self.last_check:
            self.last_check=tmp_last_check
            self.write_status()

        return result


    def get_last_check(self,ce):
        if ce.has_key('updated'):
            result= self.progressive(ce['title']) #  ce['title'][:8]#: 'DSA-4987>self.last_check:
        elif ce.has_key('published'):
            result = self.iso_date(ce['published_parsed'])
        return result

    def iso_date(self,s):
        # s="time.struct_time(tm_year=2021, tm_mon=10, tm_mday=15, tm_hour=14, tm_min=12, tm_sec=50, tm_wday=4, tm_yday=288, tm_isdst=0)"
        # print("iso_date: " + str(s))
        x=re.sub('tm_[a-z]+=','',str(s))
        x = re.sub('\(', '((', x)
        x = re.sub('\)', '))', x)
        t1=eval(x)
        result=time.strftime('%Y-%m-%dT%H:%M:%SZ', t1)
        return result

    def progressive(self,s):
        prog_str = ""
        m = re.match(r'(DSA-)(\d+)', s)
        if m!=None and len(m.groups())==2:
            prg=m.group(2)
            prog_str = prg.rjust(8, '0')
        return prog_str

    def write_status(self):
        fn=os.path.join(os.path.dirname(sys.argv[0]),self.name+".chk")
        with open(fn, 'wt') as file:
            file.write(self.last_check)

    def read_status(self):
        fn = os.path.join(os.path.dirname(sys.argv[0]),self.name + ".chk")
        try:
            with open(fn,'rt') as file:
                self.last_check=file.read()
        except:
            self.last_check=""

if __name__ == '__main__':
    dsa =  "https://www.debian.org/security/dsa-long"
    planet =  "https://planet.debian.org/rss20.xml"

    if len(sys.argv)>=2:
        url=sys.argv[1]
    else:
        url=planet
    #print(get_feeds(url))

